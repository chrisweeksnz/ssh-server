FROM alpine:latest
LABEL maintainer="Chris Weeks <https://gitlab.com/chrisweeksnz/ssh-server>" 
RUN apk update && \
    apk add --no-cache \
        bash \
        bash-completion \
        bash-doc \
        curl \
        docker-bash-completion \
        git-bash-completion \
        iproute2-bash-completion \
        jq \
        lastpass-cli \
        lastpass-cli-bash-completion \
        lastpass-cli-doc \
        less \
        less-doc \
        libvirt-bash-completion \
        man \
        man-pages \
        mdocml-apropos \
        openssh \
        openssh-doc \
        openvswitch-bash-completion \
        pv \
        shadow \
        util-linux \
        util-linux-bash-completion && \
        echo 'unicode="YES"' > /etc/rc.conf
RUN useradd --create-home --gid ${SSH_GID} --groups wheel --shell /bin/bash --uid ${SSH_UID} --user-group ${SSH_USER} && \
        echo ${SSH_USER}:${SSH_PASS} | chpass
ENV PAGER=less
EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]

# sshd config is required

# need to have label that describes sshd host keygen, as a bootstrap action?

# security audit is required

# bashrc is required
# - source /etc/profile.d/bash_completion.sh
# - aliases

# docker build --build-arg SSH_USER= --build-arg SSH_PASS= --build-arg SSH_UID= --build-arg SSH_GID= --file alpine.dockerfile --tag registry.gitlab.com/chrisweeksnz/ssh-server .
# eg.
#   docker build --build-arg SSH_USER=chris --build-arg SSH_PASS=blahblahblah261688416 --build-arg SSH_UID=501 --build-arg SSH_GID=501 --file alpine.dockerfile --tag registry.gitlab.com/chrisweeksnz/ssh-server .
#   docker build --build-arg SSH_USER=chrisweeks --build-arg SSH_PASS=blahblahblah2848 --build-arg SSH_UID=1000 --build-arg SSH_GID=1000 --file alpine.dockerfile --tag registry.gitlab.com/chrisweeksnz/ssh-server .

# https://wiki.alpinelinux.org/wiki/How_to_get_regular_stuff_working